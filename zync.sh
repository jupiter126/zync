#!/usr/local/bin/bash
## Requires bash, syncoid and mbuf
####################################
# Copyright 2021 Nelson-Jean Gaasch
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
####################################
#Declare variables
# $pport, $kkey, $uuser, $rremote and ${datasets[0]} are defined in hosts/$nname/config
nname="$1"   # nname must have been initialised first
aaction="$2" # IN or OUT
eror="$3"    # only 2 params

#Check variables:
if [[ "$nname" = "" ]] || [[ "$eror" != "" ]]; then echo "Usage is ./zync.sh nname [IN/OUT]" && exit 1; fi
if [[ "$aaction" != "IN" ]] && [[ "$aaction" != "OUT" ]]; then echo "aaction is either IN or OUT!" && exit 2; fi
if [[ -d "hosts/$nname" ]]; then
	if ! source "hosts/$nname/config"; then
		echo "config for name does not exist" && exit 3
	fi
	if [[ "$pport" = "" ]]; then echo "pport missing in conf" && exit 4
	elif [[ "$kkey" = "" ]]; then echo "kkey in conf" && exit 5
	elif [[ ! -f "$kkey" ]]; then echo "kkey file not detected in conf" && exit 6
	elif [[ "$uuser" = "" ]]; then echo "uuser missing in conf" && exit 7
	elif [[ "$rremote" = "" ]]; then echo "rremote missing in conf" && exit 8
	elif [[ "${datasets[*]}" = "" ]]; then echo "datasets missing in conf" && exit 9
	fi
else
	echo "There is no hosts/$nname directory" && exit 10
fi

#Data sync:
for dataset in "${datasets[@]}"; do
	here=${dataset// *}
	there=${dataset//* }
	if [[ "$aaction" = "IN" ]]; then
		syncoid --sendoptions="w" --sshport="$pport" --sshkey "$kkey" "$uuser@$rremote:$there" "$here"
	elif [[ "$aaction" = "OUT" ]]; then
		syncoid --sendoptions="w" --sshport="$pport" --sshkey "$kkey" "$here" "$uuser@$rremote:$there"
	fi
done
exit 0
