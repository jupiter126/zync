# README #

As I recently swapped to ZFS for my daily use on various computers, I also revised my way of doing backups.

I travel and use computer1 for a few months, then I move again and use computer2 for a few weeks, then computer3 for a few months, back to 2, back to 1, computer4 for a day, computer3 for a month....
In this particular use case, I use syncoid to sync snapshots to a central server when I work or before I leave a place.
When I get to another computer, I use syncoid to sync the local zfs to the server - syncing back up when I leave.

As source and destination permute depending on wether I arrive or leave a place, cron automation sounds bad, as It would certainly get it wrong.
Hence this little script, that uses syncoid (but not sanoid) to sync my filesystems when I travel.

Note: All the filesystems I sync are crypted, both locally (wherever that is) and on central server.
This makes things [much easier](https://zfsonlinux.topicbox.com/groups/zfs-discuss/Tc9acf1bc1513ea21-M2f7977ea237e2f536b967a84) and more secure.

### Usage ###

First, configure the host reference:

cp -R hosts/example hosts/hostref
nano hosts/hostref/config
(setup the ssh key manually)

I implement keys with a password, as the program should be run only manually and by only by me.
If the program is supposed to run alone and on predefined intervals, your might be better of with sanoid!

./zync IN hostref# inbound: gets all fs from the server
./zync OUT hostref# outbound: puts all fs on the server

So when I get in a place, I do
./zync IN hostref
And before I get out, I do 
./zync OUT hostref

hostref being my backup server

### Pros and cons: ###

It's quite simple, there is not much code, it doesn't do stuff automagically.
